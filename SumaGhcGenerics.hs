{-# language TypeOperators #-}
{-# language FlexibleInstances, FlexibleContexts #-}
{-# language DefaultSignatures #-}
module SumaGhcGenerics where

import GHC.Generics

class Suma a where
  suma :: a -> Integer

  default suma :: (Generic a, GSuma (Rep a)) => a -> Integer
  suma = gsuma . from

instance Suma Integer where
  suma = id
instance Suma Int where
  suma = toInteger

class GSuma f where
  gsuma :: f a -> Integer

instance GSuma U1 where
  gsuma _ = 0
instance Suma t => GSuma (K1 r t) where
  gsuma (K1 x) = suma x
instance (GSuma f, GSuma g) => GSuma (f :+: g) where
  gsuma (L1 x) = gsuma x
  gsuma (R1 x) = gsuma x
instance (GSuma f, GSuma g) => GSuma (f :*: g) where
  gsuma (x :*: y) = gsuma x + gsuma y

instance (GSuma f) => GSuma (M1 t c f) where
  gsuma (M1 x) = gsuma x

instance Suma a => Suma [a]
instance Suma a => Suma (Maybe a)