default : slides.pdf

%.pdf : %.md
	pandoc -t beamer $< -o $@ --latex-engine=xelatex --template beamer-template.tex

clean :
	rm *.pdf
