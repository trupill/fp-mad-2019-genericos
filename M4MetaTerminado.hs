{-# language TypeFamilies, TypeOperators #-}
{-# language DataKinds #-}
{-# language ScopedTypeVariables, TypeApplications #-}
{-# language FlexibleInstances #-}
module M4MetaTerminado where

import GHC.TypeLits
import Data.Proxy

class Generico t where
  type Repr t
  uniformizar :: t -> Repr t
  recrear     :: Repr t -> t

newtype Un a = Un a deriving Show
infixr 6 `YAdemas`
data a `YAdemas`  b = a `YAdemas` b deriving Show

data Soso = Soso
infixr 5 `OYaSiEso`
data a `OYaSiEso` b = ElUno a | ElOtro b deriving Show

newtype Tipo        (s :: Symbol) a = Tipo        a
newtype Constructor (s :: Symbol) a = Constructor a
newtype Campo       (s :: Symbol) a = Campo       a

class GInfoTipo a where
  infoTipo :: a -> [String]

instance {-# OVERLAPS #-} (GInfoTipo x, KnownSymbol t) => GInfoTipo (Tipo t x) where
  infoTipo (Tipo x :: Tipo s x) = ("Tipo: " ++ symbolVal (Proxy @s)) : infoTipo x
instance {-# OVERLAPS #-} (GInfoTipo x, KnownSymbol c) => GInfoTipo (Constructor c x) where
  infoTipo (Constructor x :: Constructor s x) = ("Constructor: " ++ symbolVal (Proxy @s)) : infoTipo x
instance {-# OVERLAPPABLE #-} GInfoTipo x where
  infoTipo _ = []