{-# language TypeFamilies, TypeOperators #-}
module M2Terminado where

-- data Either a b = Left a | Right b
-- data Maybe a = Nothing | Just a

data Edad = Edad Int
data EstadoCivil = Soltero | Arrejuntado
data Persona = Persona { nombre :: String
                       , edad   :: Edad
                       , ecivil :: Maybe EstadoCivil }

class Generico t where
  type Repr t
  uniformizar :: t -> Repr t
  recrear     :: Repr t -> t

-- 1. Representacion de Edad

newtype Un a = Un a deriving Show

instance Generico Edad where
  type Repr Edad = Un Int
  uniformizar (Edad x) = Un x
  recrear     (Un x)   = Edad x

-- 2. Representacion de Persona

infixr 6 `YAdemas`
data a `YAdemas`  b = a `YAdemas` b deriving Show

instance Generico Persona where
  type Repr Persona = Un String `YAdemas` Un Edad `YAdemas` Un (Maybe EstadoCivil)
  uniformizar (Persona n e c) = Un n `YAdemas` Un e `YAdemas` Un c
  recrear (Un n `YAdemas` Un e `YAdemas` Un c) = Persona n e c

-- 3. Representacion de Either

infixr 5 `OYaSiEso`
data a `OYaSiEso` b = ElUno a | ElOtro b deriving Show

instance Generico (Either a b) where
  type Repr (Either a b) = Un a `OYaSiEso` Un b
  uniformizar (Left  x) = ElUno  (Un x)
  uniformizar (Right y) = ElOtro (Un y)
  recrear (ElUno  (Un x)) = Left  x
  recrear (ElOtro (Un y)) = Right y

-- 4. Representacion de Maybe y EstadoCivil

data Soso = Soso

instance Generico (Maybe a) where
  type Repr (Maybe a) = Soso `OYaSiEso` Un a
  uniformizar Nothing  = ElUno Soso
  uniformizar (Just x) = ElOtro (Un x)
  recrear (ElUno  Soso)   = Nothing
  recrear (ElOtro (Un x)) = Just x

instance Generico EstadoCivil where
  type Repr EstadoCivil = Soso `OYaSiEso` Soso
  uniformizar Soltero     = ElUno Soso
  uniformizar Arrejuntado = ElOtro Soso
  recrear (ElUno  Soso) = Soltero
  recrear (ElOtro Soso) = Arrejuntado
  