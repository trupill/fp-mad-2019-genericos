{-# language TypeFamilies, TypeOperators #-}
{-# language FlexibleContexts #-}
module M3FuncionTerminado where

-- La libreria

class Generico t where
  type Repr t
  uniformizar :: t -> Repr t
  recrear     :: Repr t -> t

newtype Un a = Un a deriving Show
infixr 6 `YAdemas`
data a `YAdemas`  b = a `YAdemas` b deriving Show

data Soso = Soso
infixr 5 `OYaSiEso`
data a `OYaSiEso` b = ElUno a | ElOtro b deriving Show

-- Igualdad generica

class GIgual a where
  gigual :: a -> a -> Bool

instance (GIgual a, GIgual b) => GIgual (a `YAdemas` b) where
  gigual (x1 `YAdemas` y1) (x2 `YAdemas` y2)
    = gigual x1 x2 && gigual y1 y2

instance GIgual Soso where
  gigual _ _ = True

instance (GIgual a, GIgual b) => GIgual (a `OYaSiEso` b) where
  gigual (ElUno  x) (ElUno  y) = gigual x y
  gigual (ElOtro x) (ElOtro y) = gigual x y
  gigual _          _          = False

instance Eq t => GIgual (Un t) where
  gigual (Un x) (Un y) = x == y

-- Funcion wrapper

gigual' :: (Generico t, GIgual (Repr t))
        => t -> t -> Bool
gigual' x y = gigual (uniformizar x) (uniformizar y)