module M1EqTerminado where

data Edad = Edad Int

data EstadoCivil = Soltero | Arrejuntado

-- data Maybe a = Nothing | Just a

data Persona = Persona { nombre :: String
                       , edad   :: Edad
                       , ecivil :: Maybe EstadoCivil }

-- Instancias de Eq a mano

instance Eq Edad where
  Edad x == Edad y = x == y

instance Eq EstadoCivil where
  Soltero     == Soltero     = True
  Arrejuntado == Arrejuntado = True
  _           == _           = False
