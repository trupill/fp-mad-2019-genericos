module M1Eq where

data Edad = Edad Int

data EstadoCivil = Soltero | Arrejuntado

-- data Maybe a = Nothing | Just a

data Persona = Persona { nombre :: String
                       , edad   :: Edad
                       , ecivil :: Maybe EstadoCivil }