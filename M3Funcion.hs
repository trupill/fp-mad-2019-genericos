{-# language TypeFamilies, TypeOperators #-}
module M3Funcion where

-- La libreria

class Generico t where
  type Repr t
  uniformizar :: t -> Repr t
  recrear     :: Repr t -> t

newtype Un a = Un a deriving Show
infixr 6 `YAdemas`
data a `YAdemas`  b = a `YAdemas` b deriving Show

data Soso = Soso
infixr 5 `OYaSiEso`
data a `OYaSiEso` b = ElUno a | ElOtro b deriving Show

-- Igualdad generica

class GIgual a where
  gigual :: a -> a -> Bool

instance GIgual (a `YAdemas` b) where
  gigual x y = undefined

instance GIgual (a `OYaSiEso` b) where
  gigual x y = undefined

instance GIgual Soso where
  gigual x y = undefined

instance GIgual (Un t) where
  gigual x y = undefined