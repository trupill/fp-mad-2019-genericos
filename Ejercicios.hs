{-# language TypeFamilies, TypeOperators #-}
module Ejercicios where

class Generico t where
  type Repr t
  uniformizar :: t -> Repr t
  recrear     :: Repr t -> t

newtype Un a = Un a deriving Show
infixr 6 `YAdemas`
data a `YAdemas`  b = a `YAdemas` b deriving Show

data Soso = Soso
infixr 5 `OYaSiEso`
data a `OYaSiEso` b = ElUno a | ElOtro b deriving Show

-- PRIMER CONJUNTO DE EJERCICIOS
-- Escribir las instancias de Generico para

data List a = Nil | Cons a (List a)

instance Generico (List a) where
  type Repr (List a) = ()
  uniformizar x = undefined
  recrear x = undefined

data TwoThreeTree a
  = Leaf  a
  | Two   (TwoThreeTree a) (TwoThreeTree a)
  | Three (TwoThreeTree a) (TwoThreeTree a) (TwoThreeTree a)

instance Generico (TwoThreeTree a) where
  type Repr (TwoThreeTree a) = ()
  uniformizar x = undefined
  recrear x = undefined

-- SEGUNDO CONJUNTO DE EJERCICIOS
-- Escribir las siguientes funciones genericas

class GTamano a where
  gtamano :: a -> Integer

class GMenorQue a where
  gmenorQue :: a -> a -> Bool

class GSuma a where
  gsuma :: a -> Integer