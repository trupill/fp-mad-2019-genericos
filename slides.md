---
author: Alejandro Serrano Mena
title: Genéricos Castizos
subtitle: Junio de 2019, Madrid
theme: metropolis
monofont: Ubuntu Mono
---

# Bienvenidos

## Data type-generic programming

Técnicas para escribir funciones que operen sobre **multitud de tipos de datos** teniendo en cuenta la **estructura** del mismo

. . .

* `id x = x` **no** usa genéricos
    * Es *polimorfismo paramétrico*, no usa la estructura del tipo
* La función `(==)` **sí** lo es

## Menú del día

Bloques de teoría + ejercicios

1. Representando de tipos de datos
2. Escribiendo funciones genéricas
3. Usando metadatos

---

![anuncios](anuncios.jpg) \

---

## Escribo libros

![Book of Mondas](bookmonads.jpg) \ ![Practical Haskell](practicalhs.jpg) \

---

## Trabajo en Utrecht

*Summer School on Advanced Functional Programming* $\quad \qquad$ 26 al 30 de agosto / [afp.school](afp.school)

![Paisaje en Utrecht](utrecht.jpg) \


# Introducción y ejemplos

## Tipos a representar

```haskell
data Edad = Edad Int

data EstadoCivil = Soltero | Arrejuntado

data Maybe a = Nothing | Just a

data Persona = Persona { nombre :: String
                       , edad   :: Edad
                       , ecivil :: Maybe EstadoCivil }
```

---

## ¡Hora del código!

Vamos a escribir `(==)` para estos tipos de datos

. . .

* Es terriblemente aburrido, siempre
    * Compruebo que el constructor es el mismo
    * Comparo cada campo recursivamente
* De hecho, Haskell incluye `deriving Eq`

. . .

`(==)` es el arquetipo de programación genérica

* ... y será nuestro primer ejemplo

---

## Idea base de la programación genérica

Usar una representación uniforme para cualquier dato

$$
\begin{array}{lcl}
\multirow{2}{*}{\texttt{T}}
&
\xlongrightarrow{\texttt{uniformizar}}
&
\multirow{2}{*}{\texttt{Repr T}} \\
&
\xlongleftarrow[\quad\texttt{recrear}\quad]{}
&
\end{array}
$$

. . .

Mejor que las funciones sean inversas la una de la otra

```haskell
recrear (uniformizar x) == x
```

---

## Cómo funciona un algoritmo genérico

$$
\begin{array}{rcl}
\texttt{valor} & \texttt{::} & \texttt{T} \\
& \downarrow & {\small \texttt{uniformizar}} \\
\textsf{repr. uniforme de \texttt{valor}} & \texttt{::} & \texttt{Repr T} \\
& \downarrow & {\small \textsf{función genérica}} \\
\textsf{(repr. uniforme de) \texttt{resultado}} & \texttt{::} & \texttt{S} \\
& \downarrow & {\small \textsf{\texttt{recrear} (si \texttt{S = Repr R})}} \\
\texttt{resultado} & \texttt{::} & \texttt{R}
\end{array}
$$

---

## Cómo funciona un algoritmo genérico

$$
\begin{array}{rcl}
\texttt{p1} & \texttt{::} & \texttt{Persona} \\
\texttt{p2} & \texttt{::} & \texttt{Persona} \\
& \downarrow & {\small \texttt{uniformizar}} \\
\texttt{pg1} & \texttt{::} & \texttt{Repr Persona} \\
\texttt{pg2} & \texttt{::} & \texttt{Repr Persona} \\
& \downarrow & {\small \textsf{igualdad genérica}} \\
\texttt{resultado} & \texttt{::} & \texttt{Bool}
\end{array}
$$

---

## Librerías hasta donde alcanza la vista

Hay muchas librerías para genéricos en GHC

* Se diferencian en la representación
* Como resultado, soportan diferentes tipos de datos

. . .

* `GHC.Generics`, `generic-deriving`
* `generics-sop`, `generics-mrsop`
* `regular`, `multirec`
* `kind-generics`
    * Soporta GADTs
    * La he escrito yo en su mayor parte 8-)


# `castizo-generics`

## `castizo-generics`

Una librería más en el vasto mundo de los genéricos

* Simplificación de `GHC.Generics`

. . .

\vspace{0.5cm}

```haskell
class Generico t where
  type Repr t
  uniformizar :: t -> Repr t
  recrear     :: Repr t -> t
```

---

## Familias de tipos (asociadas)

```haskell
class Generico t where
  type Repr t
```

Cada instancia de `Generico` debe declarar un tipo para `Repr`

. . .

Las familias de tipos en Haskell pueden ser:

* Asociadas o no
    * Dependendiendo de si son parte de una clase de tipos
* Abiertas o cerradas
    * Una abierta permite ir añadiendo instancias
    * Una cerrada se comporta como una función entre tipos

---

## ¡Hora del código!

Vamos a implementar `castizo-generics`

---

## Sé lo que estáis pensando...

* ¿Para qué sirve `Un`?
* ¿No podríamos haber usado tuplas en vez de `YAdemas`?

. . .

Nos conviene tener un **conjunto propio** de tipos de datos
que usemos **exclusivamente** para la representación uniforme

---

## Genéricos *shallow*

```haskell
type Repr (Maybe a) = Soso `OYaSiEso` Un a

-- en vez de
type Repr (Maybe a) = Soso `OYaSiEso` Rep a
-- o algo similar
```

. . .

* Nos permite elegir el nivel de conversión
* La mayor parte de tipos primitivos (`Char`, `Int`) no son
  instancias de `Generico`, así que esto nos reduciría la
  cantidad de tipos de datos que podemos representar

---

## Ejercicios

Escribir instancias de `Generico` para:

```haskell
data List a = Nil | Cons a (List a)

data TwoThreeTree a
  = Leaf  a
  | Two   (TwoThreeTree a) (TwoThreeTree a)
  | Three (TwoThreeTree a) (TwoThreeTree a) (TwoThreeTree a)
```

`https://gitlab.com/trupill/fp-mad-2019-genericos
  /blob/master/Ejercicios.hs`


# Funciones genéricas

## Definir una función genérica

Tenemos que definir como funciona cada bloque

* `Un`, `YAdemas`, `Soso`, `OYaSiEso`

. . .

... y cada bloque tiene un tipo distinto, así que ...

. . .

\vspace{0.5cm}

### ¡Clases de tipos!

---

## Ejemplo: igualdad genérica

```haskell
class GIgual a where
  gigual :: a -> a -> Bool
```

Tenemos que ir creando instancias para cada bloque

---

## Caso `Un`

```haskell
instance GIgual (Un t) where
  igual (Un x) (Un y) = ??
```

. . .

```haskell
instance Eq t => GIgual (Un t) where
  igual (Un x) (Un y) = x == y
```

---

## Patrón para funciones genéricas

Podemos ver esto como un **patrón de diseño**:

* Creamos una clase para la función genérica

    ```haskell
    class GFuncion a where gfuncion :: ..
    ```

* Los casos `YAdemas`, `OYaSiEso` y `Soso` la usan recursivamente

    ```haskell
    instance GFuncion Soso
    instance (GFuncion a, GFuncion b)
      => GFuncion (a `YAdemas`  b)
    instance (GFuncion a, GFuncion b)
      => GFunction (a `OYaSiEso` b)
    ```

* El caso `Un` llama a la versión **no** genérica

    ```haskell
    instance Funcion t => GFuncion (Un t) where
       gfuncion (Un x) ... = funcion x ...
    ```

---

## Ocultando la implementación genérica

Habitualmente no quieres que el programador tenga que
llamar manualmente a la implementación genérica

1. Implementar una función *wrapper*
2. Incluir la implementación por defecto en una clase de tipos

---

## Ocultando con una función *wrapper*

```haskell
gigual' :: (Generico t, GIgual (Repr t))
        => t -> t -> Bool
gigual' x y = gigual (uniformizar x) (uniformizar y)
```

---

## Implementación por defecto en una clase de tipos

Haskell 98/2010 permite incluir una implementación por defecto
de un método de una clase, que será usada si no se define
explícitamente

```haskell
class Igual a where
  igual    :: a -> a -> Bool
  distinto :: a -> a -> Bool
  distinto x y = not (igual x y)
```

**¡Ojo!** la implementación tiene que valer para **todos**
los tipos que puedan implementar `igual`

---

## Implementación por defecto restringida

`DefaultSignatures` nos permite dar una definición por defecto,
pero sólo cuando otros requisitos se cumplen

```haskell
class Igual a where
  igual :: a -> a -> Bool

  default igual :: (Generico a, GIgual (Repr a))
                 => a -> a -> Bool
  igual x y = gigual (uniformizar x) (uniformizar y)
```

---

## Usando la implementación por defecto

Aún así, tenemos que instanciar la clase explícitamente

* Usando una `instance` vacía

    ```haskell
    instance Igual EstadoCivil
    instance Igual a => Igual (Maybe a)
    instance Igual Persona
    ```

* Usando la extensión `DeriveAnyClass`

    ```haskell
    data Persona = ... deriving Igual
    ```

---

## Ejercicios

Escribir las siguientes funciones genéricas:

* `tamaño`: cuenta el número de constructores
* `menorEq`: compara dos valores, como `(<=)` de `Ord`
* `suma`: suma todos los `Integer` que haya en un valor

`https://gitlab.com/trupill/fp-mad-2019-genericos
  /blob/master/Ejercicios.hs`

# `GHC.Generics`

## Conversión `castizo-generics` <-> `GHC.Generics`

| `castizo`     | `GHC.Generics`  |
|---------------|-----------------|
| `Soso`        | `U1`            |
| `Un t`        | `K1 R t`        |
| `YAdemas`     | `(:*:)`         |
| `OYaSiEso`    | `(:+:)`         |
| `Generico`    | `Generic`       |
| `Repr :: *`   | `Rep :: * -> *` |
| `uniformizar` | `from`          |
| `recrear`     | `to`            |

---

## Derivación automática de `Generic` 

¡No hay que escribir instancias a mano!

```haskell
{-# language DeriveGeneric #-}
import GHC.Generics

data Persona = ... deriving Generic
```

# Metadatos

## Una representación algo olvidadiza

**Problema**: cómo implementar `Show`, `Read`, `ToJson`
usando sólo los bloques que hemos introducido

* Nuestra representación uniforme **olvida** los nombres
  de los tipos y los constructores

. . .

**Solución**: nuevos bloques que lo recuerden

---

## `Symbol` y la promoción de tipos

```haskell
newtype Tipo (s :: Symbol) a = Tipo a

type Repr Persona = Tipo "Persona" (...)
```

* Un `Symbol` es un `String` a nivel de tipos
    * GHC **promociona** los constructores normales a tipos
    * Se necesita la extensión `DataKinds`
* Para reflejar su valor en tiempo de ejecución usamos

    ```haskell
    symbolVal :: KnownSymbol s => proxy s -> String
    ```

---

## Metadatos en `GHC.Generics`

Un único tipo para cualquier metadato

```haskell
newtype M1 i (c :: Meta) f p = M1 { unM1 :: f p }
```

* Sólo necesitamos una instancia en aquellas funciones genéricas
  que ignoran los metadatos
* `Meta` contiene mucha información sobre los tipos

    ```haskell
    data Meta
       = MetaData Symbol Symbol Symbol Bool
       | MetaCons Symbol FixityI Bool
       | MetaSel (Maybe Symbol) SourceUnpackedness
                 SourceStrictness DecidedStrictness
    ```

# Conclusión

## Conclusión

La programación genérica nos permite describir algoritmos genéricos
pero que dependen de la estructura del tipo de datos en cuestión

* Igualdad, comparación, serialización
* Suelen ser independentes del dominio

. . .

Los genéricos en Haskell son completamente *type-safe*

* No como el *reflection* en otros lenguajes
* Muestran el poder del sistema de tipos de GHC

---

## *Call to action*: `simplistic-generics`

*Generic programming without too many type classes*

```haskell
gigual :: OnLeaves Eq f => SRep f -> SRep f -> Integer
gigual S_U1        S_U1      = True
gigual (S_L1 x)   (S_L1 y)   = gigual x y
gigual (S_R1 x)   (S_R1 y)   = gigual x y
gigual (x :**: y) (u :**: v) = gigual x u && gigual y v
gigual (S_M1 x)   (S_M1 y)   = gigual x y
gigual (S_K1 x)   (S_K1 y)   = x == y
gigual _          _          = False
```

. . .

Para mentes interesadas en Haskell avanzado:

* `SRep` se define como un GADT
* `OnLeaves` es una familia de tipos que devuelve *constraints*
* Conversión `GHC.Generics` $\iff$ `SRep` es a su vez genérica

---

## Para curiosos: `[a]` en varias librerías

```haskell
-- GHC.Generics
type Rep [a] = U1 :+: K1 R a :*: K1 R [a]
-- Para tipos de kind * -> *
type Rep1 [] = U1 :+: Par1 :*: Rec1 []

-- generics-sop: lista de listas a nivel de tipo
type Code [a] = '[ '[], '[ a, [a] ] ]

-- regular, multirec: recursion explicita (fixpoint)
type PF [a] = U :+: I :*: R
-- data ListF a r = NilF | ConsF a r

-- kind-generics: cualquier kind y GADTs
type RepK [] = U1 :+: Field V0 :*: Field ([] :@: V0)
```