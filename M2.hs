{-# language TypeFamilies, TypeOperators #-}
module M2 where

-- data Either a b = Left a | Right b
-- data Maybe a = Nothing | Just a

data Edad = Edad Int
data EstadoCivil = Soltero | Arrejuntado
data Persona = Persona { nombre :: String
                       , edad   :: Edad
                       , ecivil :: Maybe EstadoCivil }

class Generico t where
  type Repr t
  uniformizar :: t -> Repr t
  recrear     :: Repr t -> t

-- 1. Representacion de Edad

instance Generico Edad where
  type Repr Edad = ()
  uniformizar x = undefined
  recrear x = undefined

-- 2. Representacion de Persona

instance Generico Persona where
  type Repr Persona = ()
  uniformizar x = undefined
  recrear x = undefined

-- 3. Representacion de Either

instance Generico (Either a b) where
  type Repr (Either a b) = ()
  uniformizar x = undefined
  recrear x = undefined

-- 4. Representacion de Maybe y EstadoCivil

instance Generico (Maybe a) where
  type Repr (Maybe a) = ()
  uniformizar x = undefined
  recrear x = undefined

instance Generico EstadoCivil where
  type Repr EstadoCivil = ()
  uniformizar x = undefined
  recrear x = undefined