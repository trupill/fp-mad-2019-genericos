{-# language TypeFamilies, TypeOperators #-}
{-# language DataKinds #-}
module M4Meta where

import GHC.TypeLits
import Data.Proxy

-- La libreria antes

class Generico t where
  type Repr t
  uniformizar :: t -> Repr t
  recrear     :: Repr t -> t

newtype Un a = Un a deriving Show
infixr 6 `YAdemas`
data a `YAdemas`  b = a `YAdemas` b deriving Show

data Soso = Soso
infixr 5 `OYaSiEso`
data a `OYaSiEso` b = ElUno a | ElOtro b deriving Show

-- Extensiones para metadatos

newtype Tipo        (s :: Symbol) a = Tipo        a
newtype Constructor (s :: Symbol) a = Constructor a
newtype Campo       (s :: Symbol) a = Campo       a

class GInfoTipo a where
  infoTipo :: a -> [String]